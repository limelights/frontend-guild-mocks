import { FlowscoreService } from "./interface";

export default class Post {
    key: number;
    flowscoreService: FlowscoreService

    constructor(key: number) {
        this.key = key
    }

    setFlowscoreService(service: FlowscoreService) {
        this.flowscoreService = service
    }

    calculate() {
        this.flowscoreService.calculateFlowscore(this)
    }
}
