import Post from "./Post";
import ActualFlowscoreService from './actualService'


jest.mock('./actualService')


describe('Mocks', () => {
    it('insists on behavior verification', () => {
        const serviceMock = new ActualFlowscoreService()
        const post = new Post(52);
        post.setFlowscoreService(serviceMock)

        post.calculate()

        expect(post.flowscoreService.calculateFlowscore).toHaveBeenCalled()
    })
})
