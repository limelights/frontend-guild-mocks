- What will we talk about?

  - A quick recap of testing things
  - The problem and a solution
  - Mocks, stubs and spies and what differs between them
  - How Jest was designed, as an abstract.
  - Classicist or mockist, which to choose and when?

- What we will not talk about

  - Jest API
  - Snapshot testing

- Recap of Unit testing, Integration Testing and End to End testing

  - Unit test, very granular
  - Integration test, how different parts of software integrates into each other
  - End to End test, how your software works from User to End of stack back to User

- Problems tests faces

  - The "world" around the test
    - As software grows it becomes harder to setup the "world"
  - Expected input === Expected output
  - Complex software

- How to fix? One of the solutions is:

  - Mocks, spies and stubs.
    - What are Test Doubles?
      - They are the "term" for any unit with interesting behavior that we manipulate in order for our test to pass
    - Certain "patterns" make more sense in a strongly typed OO paradigm
      So we will be using Typescript to illustrate the concept and look at the JavaScript way of solving the same problem.

- What is what? [1]

  - Stubs
  - Mocks
  - Spies

- Stubs

  - Insists on state verification
    - Let's see example 1

- Mocks

  - Insists on behavior verification
    - Let's see example 2
    - Can we spot the bug hiding in this test? actualService.ts#10

- Spies
  - Insists on usage verification
    - Let's see example 3

# La la land.

    - We use the `jest` test framework, which, was designed to be used in JavaScript and therefore did a lot of funky stuff.
        - [2] Jest does everything in the "jest.fn" scope.
            - Meaning when we use
                - `jest.mock()` or have `automock` enabled it will produce a mock function - which is a stub, mock and spy all in one.
                    - [3] Let's add `expect(post.flowscoreService.calculateFlowscore).not.toHaveBeenCalledWith(1)` to our example2.test.ts
            Jest blurs this line then between mock, stub and spy and makes for a simpler glossary - everyone can talk about a mock in jest
            and get a grasp of how that object behaves.

# Classicist vs Mockist?

    - Classicist uses real objects as far as possible
        - If the collaboration, between components, is easy then Honken prefers a classicist approach.
        - Tests in a classic approach allows for failure in non SUT tests if they contain the bug
            - Meaning if a Client class contains a bug - all tests involving said clients contains the same bug and thus will fail.
            - This leads to easier spotting of a bug and which interactions this code has, but it can be tougher to fix since this bug spans
            many modules and tests.
        - "Only care" about final state, "when all of my code is run, this is what I expect to have happened".

    - Mockist uses test doubles as soon as possible
        - If the collaboration, between components, is awkward or hard then Honken prefers the mockist approach
        until the boundaries have cleared up.
        - Tests in a mockist approach fail only when the SUT contains the bug.
            - Meaning if a Client class contains a bug - only the tests that specifically tests this Client will fail.
            This could also lead to other issues, however, in which you dont know if the feature you've built or corrected will have ripple effects on other parts of the system since they, *are*, mocked out. See [3].
        - "Only care" about how this SUT has communicated with external code
            - This approach forces you to think about implementation when writing your test

Resources:

[1] https://martinfowler.com/articles/mocksArentStubs.html#TheDifferenceBetweenMocksAndStubs
[2] https://jestjs.io/docs/en/mock-functions
