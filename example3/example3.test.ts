import Post from "./Post";
import ActualFlowscoreService from './actualService'


describe('Spies', () => {
    it('insists on usage verification', () => {
        const service = new ActualFlowscoreService()
        const calculateFlowscoreSpy = jest.spyOn(service, 'calculateFlowscore')
        const post = new Post(52);
        post.setFlowscoreService(service)

        post.calculate()

        expect(calculateFlowscoreSpy).toHaveBeenCalledWith(post)
    })

    it('insists on usage verification', () => {
        const service = new ActualFlowscoreService()
        const calculateFlowscoreSpy = jest.spyOn(service, 'calculateFlowscore')
        const post = new Post(1337);
        post.setFlowscoreService(service)

        expect(() => {
            post.calculate()
        }).toThrowError(/Cant calculate/)

        expect(calculateFlowscoreSpy).toHaveBeenCalledWith(post)
    })
})
