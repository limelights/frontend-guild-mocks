import { FlowscoreService } from "./interface";
import Post from "./Post";

export default class ActualFlowscoreService implements FlowscoreService {
    #flowscore: number = 0
    calculateFlowscore(post: Post): void {
        if (post.key === 52) {
            this.#flowscore = post.key + 1;
        } else {
            throw new Error('Cant calculate');
        }
    }

    getCalculatedFlowscore(): number {
        return this.#flowscore;
    }
}
