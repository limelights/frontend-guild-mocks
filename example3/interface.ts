import Post from "./Post";

export interface FlowscoreService {
    calculateFlowscore(post: Post): void;
}
