import { FlowscoreServiceStub, Post } from "./example1";

describe('Stubs', () => {
    it('insists on verifying state', () => {
        const post = new Post(51);
        const flowscoreServiceStub = new FlowscoreServiceStub();
        post.setFlowscoreService(flowscoreServiceStub);
        post.calculate();
        expect(flowscoreServiceStub.getCalculatedFlowscore()).toBe(52);
    })
})
