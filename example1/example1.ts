class Post {
    key: number;
    flowscoreService: FlowscoreService

    constructor(key: number) {
        this.key = key
    }

    setFlowscoreService(service: FlowscoreService) {
        this.flowscoreService = service
    }

    calculate() {
        this.flowscoreService.calculateFlowscore(this)
    }
}


interface FlowscoreService {
    calculateFlowscore(post: Post): void;
    getCalculatedFlowscore(): number;
}


class FlowscoreServiceStub implements FlowscoreService {
    #flowscore: number;

    calculateFlowscore(post: Post): void {
        this.#flowscore = post.key + 1;
    }

    getCalculatedFlowscore(): number {
        return this.#flowscore;
    }
}

export {
    FlowscoreService,
    FlowscoreServiceStub,
    Post
}
